(function() {
  'use strict';

  let $ = document.querySelector.bind(document), $$ = document.querySelectorAll.bind(document);
  let bg;
  
  let selectedClass = 'mdl-button--accent';

  function init() {
    bg = $('#bg');
    let selectors = $$('button');
    selectors.forEach(el => preloadImage(el));
    selectors.forEach(el => addEvent(el));
    setBackground.call($(`.${selectedClass}`));
  }

  function preloadImage(el) {
    let img = new Image();
    img.src = el.getAttribute('image');
  }

  function addEvent(el) {
    el.addEventListener('click', setBackground);
  }

  function setBackground() {
    bg.style.backgroundImage = `url(${this.getAttribute('image')})`;
    $$(`.${selectedClass}`).forEach(el => el.classList.remove(selectedClass));
    this.classList.add(selectedClass);
  }

  window.onload = init;
})();
